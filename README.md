## Completed
- Validate token in every request 
- Prevent all unauthenticated requests to our services.
- Configure routing

## Pending 

- Comments
- Testing in JUnit 

